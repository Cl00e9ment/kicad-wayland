pkgname=kicad-wayland
pkgver=7.0.7
pkgrel=1
pkgdesc='Electronic schematic and printed circuit board (PCB) design tools - patched for native Wayland support'
arch=('x86_64')
url='http://kicad.org/'
license=('GPL')
depends=(
  'boost-libs'
  'curl'
  'glew'
  'glm'
  'ngspice'
  'opencascade'
  'python'
  'python-wxpython'
  'wxwidgets-gtk3'
  'unixodbc'
)
makedepends=(
  'git'
  'cmake'
  'ninja'
  'mesa'
  'boost'
  'swig'
)
optdepends=(
  'kicad-library: for footprints, symbols and templates'
  'kicad-library-3d: for 3D models of components'
)

conflicts=('kicad' 'kicad-bzr')
provides=('kicad')

source=(
  'fix-archive-permissions.patch'
  'make-kicad-work-on-native-wayland.patch'
)
sha512sums=('e36912e4144c6debe33f1711ca7a2da020389a6e376282c70f534ce9e685d0b35b58d88b810327d8baeb3466e87eda246ebf181c18e134e80bc73b91dae4bf02'
            'b68da61ececf8d7a5853f6f954a78474ae73f2213b770e15a7f18c294876013ea1499aac7fb54bd8f7b52aebd5b88dbb8678f45c548e006e733a44ab4c614882')

prepare() {
  REPO_URL='https://gitlab.com/kicad/code/kicad.git'
  LATEST_RELEASE_TAG="$(
    git -c 'versionsort.suffix=-' ls-remote --tags --refs --sort='v:refname' "$REPO_URL" \
    | cut -d/ -f3 \
    | grep --color=never -E '[0-9]+\.[0-9]+\.[0-9]+' \
    | grep --color=never -v 99 \
    | tail -n 1 \
  )"

  rm -rf kicad
  git clone --depth=1 --branch "$LATEST_RELEASE_TAG" "$REPO_URL"

  cd kicad
  patch -p1 -i "${srcdir}/fix-archive-permissions.patch"
  patch -p1 -i "${srcdir}/make-kicad-work-on-native-wayland.patch"
}

pkgver() {
  git -C kicad describe
}

build() {
  cmake \
    -B build \
    -S kicad \
    -G Ninja \
    -D CMAKE_BUILD_TYPE=Release \
    -D CMAKE_INSTALL_PREFIX=/usr \
    -D KICAD_USE_OCE=ON \
    -D KICAD_USE_EGL=ON \
    -D KICAD_BUILD_I18N=ON \
    -D KICAD_I18N_UNIX_STRICT_PATH=ON \
    -D KICAD_BUILD_QA_TESTS=OFF \
    -W no-dev

  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build

  strip "$STRIP_SHARED" "${pkgdir}"/usr/lib/python*/site-packages/_pcbnew.so
}
