FROM archlinux:latest

RUN pacman -Syu --noconfirm \
	&& pacman -S --noconfirm base-devel git cmake ninja mesa boost swig \
	glew glm ngspice opencascade python python-wxpython wxwidgets-gtk3 unixodbc

RUN useradd -m builduser
WORKDIR /build
RUN chown builduser:builduser /build
USER builduser
